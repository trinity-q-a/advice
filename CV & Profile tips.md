### Introduction

The file was created as the result of discussion on [building effective profiles](https://gitlab.com/trinity-q-a/advice/-/issues/5). Most of the content here was provided by [Dmitri Zaitsev](https://www.maths.tcd.ie/~zaitsev/). Feel free to add more information and give your feedback. 

### CV (Resume)

1. When there are lots of applications, recruiters only have around 30 seconds to assess a candidate. A detailed CV might distract them from your key qualities. For instance, Google recommends a one-page CV, which highlights key qualifications you have for the job.

2. It is a good idea to provide multiple links to resources such as Gitlab/Github, LinkedIn, Devpost and etc in your CV

### Cover Letters

1. Your cover letter should be short and only contain what you don't want to include in Bio or specilized profiles.

2. For instance, if it is a job application - anything specific about the job and what you think can be directly relevant. Again - the same rules as for Bio - be brief and vague but modest and direct to the same specialized profiles for more information. And yes - be careful with "empty" words but the basic rule is actually simple - only say what you can justify with links showing concrete information about you.

### Red flags (things to avoid)

1.  Exaggerated claims that may come across as "boasting" and lack of modesty, especially if no link is provided to prove the claim. Be cautious with statements you cannot justify or provide more details. Because as people get more interested, they will want to see more details - your actual contribution to the project, impact etc.

### General Tips (Bio)

1. One-page CV is needed but remember many people don't have time even for one page. So you first need a short Bio - few paragraph highlighting your most interesting achievements. For instance, the important skill of technical documentation writing requires different rules, and to demonstrate your expertise there, you are better off saying you have X years experience and provide a link to your more specialized profile describing that experience.

2. While you can have just one Bio for everyone, the one-page CV is likely impossible to make equally appealing to people with different interests. It is not even clear what role should it play. If people have little time, they should see everything they need in your shorter Bio to decide whether they want to see more details. 

3. Most important to keep in mind - your one-page Bio need to appeal to people who don't know and have little time and maybe interest to read more. That also means you can lose them if anything looks to them like a "red flag". It is better to be vague and modest but provide easy-to-follow links for more information. Also make it clear there are more details to be found.

4. Your Github profile can be good if you have equally contributed to multiple projects. On the other hand, if your contribution is best seen on specific projects - it is better to link directly there to avoid people searching around and losing time on other pages.

5. Don't put too many links that are similar. For instance, if you have major contributions to two or more projects, say "contributed to several projects" with only one link - to your repository listing your main projects.

6. Don't write long sentences unless you are creative writer. A clean itemized list instead will better show your organisation skills that many people may value.

7. A shorter Bio is also safer - less chance for "red flags". Many CVs have personal details parts mixed with professional experience wasting everyone's time. Separate the two as clear as possible.

### Volunteering

1. Volunteer activity is increasingly gaining attention and appreciation but again, saying you worked for that project is not as convincing as reading there about your actual contribution or seeing some stats if your contribution is spread over many different parts.

