### Introduction 

We decided to create this file because people often have misconceptions about [arxiv.org](https://arxiv.org/). 

### What is [arxiv.org](https://arxiv.org/)?

Arxiv.org is a website, which hosts **preprints** of scholarly articles in technical fields such as physics, mathematics, economics and computer science. It is a non-profit organization that is run by [Cornell University](https://www.cornell.edu/).

### A few facts about [arxiv.org](https://arxiv.org/)

1. [arxiv.org](https://arxiv.org/) only stores preprints, which may or may not get published, often after some changes made. It is a **good practice** to update your preprint on Arxiv to the published version, and to add the journal reference to its Arxiv-metadata, but not everyone does it.

2. The content on [arxiv.org](https://arxiv.org/) is not peer-reviewed and just needs to pass a moderation process to get published in the system. The information is hence provided "as it is".

3. While it is very uncommon that the people look at others' unpublished articles on arxiv.org and use the results without a proper reference it can still be the case. People can just cite your Arxiv preprint, especially if there is no reference there that it was published, which is why it is **good practice** to post it once the preprint appears in a journal

4. Once you publish the preprint (article) in arxiv.org you will not be able to remove it. If you change the article, the earlier versions will still be available on the platform. However, there is an option to withdraw your text before publishing. 

