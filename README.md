# Career & publishing advice

Sharing knowledge around job opportunities, entrepreneurship and academic/scholarly publishing, run by [TCD](https://www.tcd.ie/) students, staff and alumni but open to everyone.

#### [Principles](Our principles.md)

#### [Team behind this forum](Team.md)

#### [How to build an effective profile](CV & Profile tips.md)

#### [What is arxiv.org](arxiv.md)

## Using this site - issues and directory

### Reading issues

To see the list of all `issues`,
click on `Issues` tab in the left navigation panel (3rd icon from the top). 
You can `search`, `filter` or `sort` the `issues` by `labels`.

When you see any `issue` of interest, you can enter and add your `comments` at the bottom, 
or below each `comment` or `comment thread`.


### Email notifications
You can subscribe to `notifications` for any `issue` by `toggling` the `notification` switch
at the very bottom in the right navigation panel (you may need to `scroll down`).


### Open new issue

To start new `issue` go to the list of all `issues` as described above, 
and click the green button `New issue` in the right top corner.

You can simply add `title` and `description` (skipping all other fields), 
and click on the green `Submit issue` button.

### Files and directories
To see all `files` and `directories` stored on this site, 
click on `Repository` tab in the left navigation panel (2nd icon from the top). 

### Formatting
For advanced formatting, `headers`, `lists`, `itemization`, `images`, `videos`, `mathematics formulas` 
and much more, use the Markdown language https://docs.gitlab.com/ee/user/markdown.html
