### Welcome to Career and Publishing Advice forum

1. Never feel embarassed to ask a question. It is beneficial both to you and other members of the forum.

2. Be friendly and polite to other members of the forum. 

3. Don't worry about making mistakes when asking questions. There is always a possibility to edit your text :)

4. If there is anything you worry about, feel free to contact our [Team](https://gitlab.com/trinity-q-a/advice/-/blob/master/Team.md).

5. We wish you every success!
