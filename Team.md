# [Career advice & publishing forum](https://gitlab.com/trinity-q-a/publishing) - the team

### Founded and managed by

- [Dmitri Zaitsev](https://www.linkedin.com/in/dmitrizaitsev/) ([Trinity College Dublin, Mathematics](https://www.maths.tcd.ie/~zaitsev/); [Public Discussion on Publishing Reform, Co-founder](https://gitlab.com/publishing-reform/discussion/-/blob/master/Team.md); [Free Journal Network, Advisory Committee](https://freejournals.org/fjn-organization/governance/))
- [Pavel Petrukhin](https://www.linkedin.com/in/pavelpetrukhin/) (Trinity College Dublin)
- [Shahina Shamshodova](https://www.linkedin.com/in/shamshodova/) (Trinity College Dublin)


